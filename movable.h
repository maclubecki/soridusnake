#ifndef MOVABLE_H_INCLUDED
#define MOVABLE_H_INCLUDED
class Movable
{
    private:

        Point       position;
        Point  cur_direction;

        int vel;
        int inputs;
        int tail_count, tail_toadd;
        bool is_dead;

    public:

        int score;

        Movable();
        int getX();
        int getY();
        int getXdir();
        int getYdir();
        int getTailCount();
        int getToAdd();

        bool getIsDead();

        void setIsDead( bool _is_dead );
        void setPosition( int _x, int _y );
        void setDir( int _x, int _y );
        void setToAdd( int );
        void setTailCount( int );
        void reset();

        void handle_input( bool &quit  );
        void move( Tail*, bool, bool, Tile*, Food& );

};


#endif // MOVABLE_H_INCLUDED
