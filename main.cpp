#include "game.h"

int main( int argc, char* args[] )
{
    long tiem_old, tiem_new;
    float tiem_frame;
    float accumulator = 0.0;
    const float dt = 0.1;

    bool quit = false;
    Game g;

    if( init() == false )
        return 1;
    if( load_files() == false)
        return 1;

    tiem_old = SDL_GetTicks();
    srand( SDL_GetTicks() );


        while( quit == false)
        {
            tiem_new = SDL_GetTicks();
            tiem_frame = ( tiem_new - tiem_old )/1000.f;
            tiem_old = tiem_new;

            accumulator += tiem_frame;

            if( g.getState() == GAME_MENU )
            {
                g.startup( quit, accumulator );
            }
            if( g.getState() == GAME_PLAYING )
            {
                g.play( quit, dt, accumulator );
            }
            if( g.getState() == GAME_DEATH )
            {
                g.deathScreen( quit, dt, accumulator );
            }
            //Updating screen
            if( SDL_Flip( sf_Screen ) == -1 )
                return 1;
        }



    cleanup();
    return 0;
}
