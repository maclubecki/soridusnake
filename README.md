Another small C++/SDL project to pass time before heavy studying. Classic snake clone, one thing that makes it different mechanics-wise is the randomized levels.
![3.jpg](https://bitbucket.org/repo/XLo4GB/images/3822982756-3.jpg)
![1.jpg](https://bitbucket.org/repo/XLo4GB/images/1164219507-1.jpg)
![2.gif](https://bitbucket.org/repo/XLo4GB/images/2675464500-2.gif)