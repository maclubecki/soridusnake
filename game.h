#include "globals.h"
#include "button.h"
#include "tile.h"
#include "tail.h"
#include "movable.h"

#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED
class Game
{
    private:
        int mx, my; // mouse coords for the menu
        int msg_x;
        bool added;

        e_Gamestate gameState_current;

        Button* btn_Start;
        Button* btn_Quit;
        Button* btn_Restart;

        Tile** stage;

        Movable* player;
        Tail* dtail;
        Tail* dtail_operator;
        Food grub;

    public:
        Game();
        e_Gamestate getState();
        void setState( e_Gamestate gameState_new );

        //core functions:
        void startup( bool &quit, float &accumulator );
        void play( bool &quit, float dt, float &accumulator );
        void deathScreen( bool &quit, float dt, float &accumulator );
        void showUI();
        //tail functions:
        //pointer setters
        void setOPLast();
        void setOPFirst();
        void addSegment();
        void removeSegment();
        void clearTail();
        bool tailCollision();
        //game mechanics functions:
        void tileClear();
        void stageClear();
        void stageVisualUpdate();
        void stageGenerator();
        void stageGenerator_Lines();
        void stageGenerator_SetRespawn();
        void deathCheck();
        void genFood();
        void resetGame();
        Tile* getNextStep();
};


#endif // GAME_H_INCLUDED
