#ifndef TILE_H_INCLUDED
#define TILE_H_INCLUDED
class Tile
{
    private:
        int x, y;
        e_Tiletype type;
        SDL_Rect clip;

    public:
        Tile();
        Tile( int _x, int _y, e_Tiletype _type );
        int getX();
        int getY();
        e_Tiletype getType();
        void setX( int _x );
        void setY( int _y );
        void setType( e_Tiletype _newtype );
        void show();
};

#endif // TILE_H_INCLUDED
