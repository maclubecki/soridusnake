#include "game.h"

Game::Game()
{
    gameState_current = GAME_MENU;

    //stage init
    stage = new Tile*[STAGE_A];

    for( int i = 0; i < STAGE_A; i++ )
    {
        stage[i] = new Tile[STAGE_A];
    }

    for( int i = 0; i < STAGE_A; i++ )
    {
        for( int j = 0; j < STAGE_A; j++ )
        {
            stage[i][j].setX(i);
            stage[i][j].setY(j);
        }
    }

    //death msg init
    msg_x = -50;

    //food init
    grub.x = 0;
    grub.y = 0;
    grub.eaten = true;

    //snake init
    player = new Movable();

    dtail = NULL;//new Tail( player->getX(), player->getY() );
    dtail_operator = dtail;

    btn_Start = new Button( WID/16, HIG/2 - 70, "START GAME" );
    btn_Quit  = new Button( WID/16, HIG/2 - 10,  "QUIT GAME" );
    btn_Restart = new Button( WID/2 - 70, HIG/2 - 25, "TRY AGAIN" );

    stageGenerator();
}

e_Gamestate Game::getState()
{
    return gameState_current;
}
Tile* Game::getNextStep()
{
    Tile* pointer;
    int x, y;

    x = player->getX() + player->getXdir();
    y = player->getY() + player->getYdir();

    if( x > 0 && x < STAGE_A && y > 0 && y < STAGE_A )
        pointer = &stage[x][y];
    else
    {
        if( x < 0 )
            pointer = &stage[STAGE_A - 1][y];
        if( x > STAGE_A - 1 )
            pointer = &stage[0][y];
        if( y < 0 )
            pointer = &stage[x][STAGE_A - 1];
        if( y > STAGE_A - 1 )
            pointer = &stage[x][0];
    }

    return pointer;
}
void Game::genFood()
{
    if( !grub.eaten ) return;

    do
    {
        grub.x = random_gen( 1, STAGE_A - 1);
        grub.y = random_gen( 1, STAGE_A - 1);
    }
    while( stage[grub.x][grub.y].getType() != TILE_EMPTY );

    stage[grub.x][grub.y].setType( TILE_PICKUP );
    grub.eaten = false;
}
void Game::setState( e_Gamestate gameState_new )
{
    gameState_current = gameState_new;
}
void Game::resetGame()
{
    //death message reset
    msg_x = -50;
    //resetting everything
    clearTail();
    player->reset();
    stageGenerator();
    grub.eaten = true;
    genFood();
    //set state back to playing
    setState( GAME_PLAYING );
}
void Game::showUI()
{
    std::stringstream t;
    setOPLast();
    t << "Tail size: " << player->getTailCount();
    t << " Rations eaten: " << player->score;

    SDL_FreeSurface(sf_UI_Text);
    sf_UI_Text = TTF_RenderText_Solid( font_UI, t.str().c_str(), txtColor_white );
    apply_surface( WID - sf_UI_Text->w - 15, HIG - sf_UI_Text->h - 5, sf_UI_Text, sf_Screen );
}
void Game::setOPLast()
{
    dtail_operator = dtail;
    if( dtail_operator != NULL )
    {
        while( dtail_operator->next != NULL )
            dtail_operator = dtail_operator->next;
    }
}
void Game::setOPFirst()
{
    dtail_operator = dtail;
    if( dtail_operator != NULL )
    {
        while( dtail_operator->prev != NULL )
            dtail_operator = dtail_operator->prev;
    }
}
void Game::stageClear()
{
    setOPFirst();
    //- the tail
    while( dtail_operator != NULL )
    {
        stage[ dtail_operator->getX() ][ dtail_operator->getY() ].setType( TILE_EMPTY );
        dtail_operator = dtail_operator->next;
    }

    //- the head
    stage[ player->getX() ][ player->getY() ].setType( TILE_EMPTY );
}
void Game::stageVisualUpdate()
{
    if( !player->getIsDead() )
    {
        setOPFirst();
        //- the tail
        while( dtail_operator != NULL )
        {
            stage[ dtail_operator->getX() ][ dtail_operator->getY() ].setType( TILE_TAIL );
            dtail_operator = dtail_operator->next;
        }
        //- the head
        if( !player->getIsDead() )
            stage[ player->getX() ][ player->getY() ].setType( TILE_PLAYER );
        //- the grub
        if( !grub.eaten )
            stage[ grub.x ][ grub.y ].setType( TILE_PICKUP );
    }
}
void Game::stageGenerator()
{
    //first clear the stage
    for( int i = 0; i < STAGE_A; i++ )
        for( int j = 0; j < STAGE_A; j++ )
            stage[i][j].setType( TILE_EMPTY );

    stageGenerator_Lines();
    stageGenerator_Lines();
    stageGenerator_SetRespawn();
}
void Game::stageGenerator_SetRespawn()
{
    bool got_it = false;

    for( int i = 0; i < STAGE_A/2 ; i+=3 )
    {
        for( int j = 0; j < STAGE_A/2 ; j+=3 )
        {
            if( stage[i][j+1].getType() == TILE_EMPTY && stage[i][j+2].getType() == TILE_EMPTY && stage[i][j+3].getType() == TILE_EMPTY )
            {
                player->setPosition(i,j);
                got_it = true;
                break;
            }
        }
        if( got_it ) break;
    }
}
void Game::stageGenerator_Lines()
{
    short stage_object_size, stage_object_count, stage_object_distance, r;
    bool line_is_vertical, lines_are_paired;

    //LINES:
    //a pair or a single line?
    r = random_gen( 1, 100, SDL_GetTicks() );
    if ( r%2 == 0 ) lines_are_paired = true;
    else            lines_are_paired = false;
    //how many pieces will it have?
    stage_object_count = random_gen( 1, 3 );
    //doing verticals or horizontals?
    r = random_gen( 1 , 100, SDL_GetTicks() );
    if( r%2 == 0 ) line_is_vertical = true;
    else line_is_vertical = false;
    //distance from (0,0), if single line it goes to the center
    if( !lines_are_paired || stage_object_count == 1 ) stage_object_distance = 17;
    else stage_object_distance = random_gen( 3, 12, SDL_GetTicks() );

    switch( stage_object_count )
    {
        case 1:
            for( int i = 1; i < STAGE_A - 1; i++ )
            {
                if( line_is_vertical ) stage[stage_object_distance][i].setType( TILE_BLOCK );
                else stage[i][stage_object_distance].setType( TILE_BLOCK );
            }
            break;

        case 2:
            for( int i = 1; i < 15; i++ )
            {
                if( line_is_vertical )
                {
                    stage[stage_object_distance][i].setType( TILE_BLOCK );
                    if( lines_are_paired ) stage[STAGE_A - stage_object_distance][i].setType( TILE_BLOCK );
                }
                else
                {
                    stage[i][stage_object_distance].setType( TILE_BLOCK );
                    if( lines_are_paired ) stage[i][STAGE_A - stage_object_distance].setType( TILE_BLOCK );
                }
            }
            for( int i = 19; i < STAGE_A - 1; i++ )
            {
                if( line_is_vertical )
                {
                    stage[stage_object_distance][i].setType( TILE_BLOCK );
                    if( lines_are_paired ) stage[STAGE_A - stage_object_distance][i].setType( TILE_BLOCK );
                }
                else
                {
                    stage[i][stage_object_distance].setType( TILE_BLOCK );
                    if( lines_are_paired ) stage[i][STAGE_A - stage_object_distance].setType( TILE_BLOCK );
                }
            }
            break;

        case 3:
            for( int i = 1; i < 9; i++ )
            {
                if( line_is_vertical )
                {
                    stage[stage_object_distance][i].setType( TILE_BLOCK );
                    if( lines_are_paired ) stage[STAGE_A - stage_object_distance][i].setType( TILE_BLOCK );
                }
                else
                {
                    stage[i][stage_object_distance].setType( TILE_BLOCK );
                    if( lines_are_paired ) stage[i][STAGE_A - stage_object_distance].setType( TILE_BLOCK );
                }
            }
            for( int i = 12; i < 23; i++ )
            {
                if( line_is_vertical )
                {
                    stage[stage_object_distance][i].setType( TILE_BLOCK );
                    if( lines_are_paired ) stage[STAGE_A - stage_object_distance][i].setType( TILE_BLOCK );
                }
                else
                {
                    stage[i][stage_object_distance].setType( TILE_BLOCK );
                    if( lines_are_paired ) stage[i][STAGE_A - stage_object_distance].setType( TILE_BLOCK );
                }
            }
            for( int i = 26; i < 34; i++ )
            {
                if( line_is_vertical )
                {
                    stage[stage_object_distance][i].setType( TILE_BLOCK );
                    if( lines_are_paired ) stage[STAGE_A - stage_object_distance][i].setType( TILE_BLOCK );
                }
                else
                {
                    stage[i][stage_object_distance].setType( TILE_BLOCK );
                    if( lines_are_paired ) stage[i][STAGE_A - stage_object_distance].setType( TILE_BLOCK );
                }
            }
            break;
    }
}
void Game::addSegment()
{
    setOPLast();

    //there is no tail
    if( dtail == NULL && dtail_operator == NULL )
    {
        dtail = new Tail( player->getX(), player->getY() );
        dtail_operator = dtail;
    }
    //there is at least 1 tail segment
    else
    {
        dtail_operator->next = new Tail( dtail_operator->getX(), dtail_operator->getY() );
        dtail_operator->next->setPrevious( dtail_operator );
    }

    player->setToAdd( player->getToAdd() - 1 );
    player->setTailCount( player->getTailCount() + 1 );
}
void Game::removeSegment()
{
    setOPLast();
    if( dtail_operator->prev != NULL )
    {
        //clear stage of the bit that's about to be deleted
        stage[dtail_operator->getX()][dtail_operator->getY()].setType( TILE_EMPTY );
        //stage[dtail_operator->getX()][dtail_operator->getY()].setOccupation( false );
        //set the pointer to the bit that's before the one to be deleted
        dtail_operator = dtail_operator->prev;
        //set the last one's "prev" pointer to null
        dtail_operator->next->prev = NULL;
        //remove the last bit
        delete dtail_operator->next;
        //set the pointer to the deleted bit to null
        dtail_operator->next = NULL;

        player->setTailCount( player->getTailCount() - 1 );
    }
    else
    {
        //clear stage of the bit that's about to be deleted
        stage[dtail_operator->getX()][dtail_operator->getY()].setType( TILE_EMPTY );
        //stage[dtail_operator->getX()][dtail_operator->getY()].setOccupation( false );
        //set the pointer to the deleted bit to null
        dtail_operator->next = NULL;
        //remove the last bit
        delete dtail_operator;
        dtail = NULL;
        dtail_operator = NULL;

        player->setTailCount(0);
    }
}
void Game::clearTail()
{
    while( player->getTailCount() > 0 )
    {
        removeSegment();
    }
}
bool Game::tailCollision()
{
    int calcX = 0, calcY = 0;
    bool collision = false;

    setOPFirst();

    while( dtail_operator != NULL )
    {
        //gotta calculate the next position like this because of the 'phasing' from one end to another
        //will not work for a bordered box level
        calcX = player->getX() + player->getXdir();
        if( calcX >= STAGE_A ) calcX = 0;
        if( calcX < 0 ) calcX = STAGE_A - 1;
        calcY = player->getY() + player->getYdir();
        if( calcY >= STAGE_A ) calcY = 0;
        if( calcY < 0 ) calcY = STAGE_A - 1;

        if ( dtail_operator->getX() == calcX && dtail_operator->getY() == calcY )
        {
            collision = true;
            break;
        }

        dtail_operator = dtail_operator->next;
    }

    setOPLast();
    return collision;
}
void Game::deathCheck()
{
    if( !player->getIsDead() )
        return;

    //death:
    gameState_current = GAME_DEATH;
}
//core functions:
void Game::startup( bool &quit, float &accumulator )
{
    SDL_Event mouseEvent;

    //getting input
    if( SDL_PollEvent( &mouseEvent ) )
    {
        mx = mouseEvent.motion.x;
        my = mouseEvent.motion.y;

        if( mouseEvent.type == SDL_MOUSEBUTTONDOWN )
        {
            if( mouseEvent.button.button == SDL_BUTTON_LEFT )
            {
                if( btn_Start -> is_selected( mx, my ) )
                {
                    accumulator = 0.0;
                    setState( GAME_PLAYING );
                    return;
                }
                if( btn_Quit -> is_selected( mx, my ) )
                {
                    quit = true;
                    return;
                }
            }
        }

        if( mouseEvent.type == SDL_QUIT )
        {
            quit = true;
            return;
        }
    }
    //checking which sprite for the buttons should be used
    btn_Start -> is_selected( mx, my );
    btn_Quit -> is_selected( mx, my );

    apply_surface( 0, 0, sf_Menubg, sf_Screen );
    btn_Start -> show();
    btn_Quit -> show();
}
void Game::deathScreen( bool &quit, float dt, float &accumulator )
{
    bool giveChoice = true;
    SDL_Event mouseEvent;

    if( msg_x < WID ) giveChoice = false;

    //getting input
    if( SDL_PollEvent( &mouseEvent ) )
    {
        mx = mouseEvent.motion.x;
        my = mouseEvent.motion.y;

        if( mouseEvent.type == SDL_MOUSEBUTTONDOWN )
        {
            if( giveChoice && btn_Restart -> is_selected( mx, my ) )
            {
                resetGame();
                return;
            }
        }

        if( mouseEvent.type == SDL_QUIT )
        {
            quit = true;
            return;
        }
    }

    //using a loop with a different dt
    while( accumulator >= 0.01 )
    {
        if( !giveChoice )
            msg_x += MSG_V * 0.01;
        accumulator -= 0.01;
    }

    //display other shit like normal
    apply_surface( 0, 0, sf_Background, sf_Screen );
    showUI();

    for( int i = 0; i < STAGE_A; i++ )
        for( int j = 0; j < STAGE_A; j++ )
            stage[i][j].show();
    if( !giveChoice )
        apply_surface( msg_x , HIG/2, sf_DeathText, sf_Screen );
    else
    {
        //show the buttons here
        //checking which sprite for the buttons should be used
        btn_Restart -> is_selected( mx, my );
        btn_Restart -> show();
    }

}
void Game::play( bool &quit, float dt, float &accumulator )
{
    //INPUT:
    player->handle_input( quit );

    //MAIN LOOP:
    while( accumulator >= dt )
    {
        //LOGIC:
        added = false;
        //add segments if needed
        if( player->getToAdd() > 0 )
        {
            addSegment();
            added = true;
        }
        //clear the stage tiles before next move:
        stageClear();
        setOPLast();
        player->move( dtail_operator, added, tailCollision(), getNextStep(), grub );
        //set the stage tiles again
        stageVisualUpdate();
        deathCheck();
        genFood();

        accumulator -= dt;
    }

    //DISPLAY:
    apply_surface( 0, 0, sf_Background, sf_Screen );
    showUI();

    for( int i = 0; i < STAGE_A; i++ )
        for( int j = 0; j < STAGE_A; j++ )
            stage[i][j].show();


}