#ifndef SURFACES_H_INCLUDED
#define SURFACES_H_INCLUDED

//Surfaces:
SDL_Surface* sf_Screen = NULL;
SDL_Surface* sf_Menubg = NULL;
SDL_Surface* sf_Background = NULL;
SDL_Surface* sf_UI_Text = NULL;
SDL_Surface* sf_DeathText = NULL;
SDL_Surface* sf_Button_Sprites = NULL;
SDL_Surface* sf_Buttontxt = NULL;
SDL_Surface* sf_Tile_Sheet = NULL;
SDL_Surface* sf_Error = NULL;

//Fonts:
TTF_Font* font_UI = NULL;
SDL_Color txtColor_white = { 255, 255, 255 };

#endif // SURFACES_H_INCLUDED
