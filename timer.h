#ifndef TIMER_H_INCLUDED
#define TIMER_H_INCLUDED
class Timer
{
    private:

    int start_time;
    int paused_time;
    bool paused;
    bool running;

    public:

    Timer();

    int getTicks();

    bool isStarted();
    bool isPaused();

    void start();
    void stop();
    void pause();
    void unpause();
};


#endif // TIMER_H_INCLUDED
