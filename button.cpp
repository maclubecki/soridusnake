#include "button.h"

Button::Button( int _posX, int _posY, std::string _text )
{
    use_index = BUTTON_NORMAL;
    text = _text;

    clips[BUTTON_NORMAL].x = 0;
    clips[BUTTON_NORMAL].y = 0;
    clips[BUTTON_NORMAL].w = 140;
    clips[BUTTON_NORMAL].h = 40;

    clips[BUTTON_HIGHLIGHTED].x = 140;
    clips[BUTTON_HIGHLIGHTED].y = 0;
    clips[BUTTON_HIGHLIGHTED].w = 140;
    clips[BUTTON_HIGHLIGHTED].h = 40;

    if( _posX == 0 ) pos_x = WID/2 - clips[BUTTON_NORMAL].w/2;
    else             pos_x = _posX;
    pos_y = _posY;
}

bool Button::is_selected( int _x, int _y )
{
    if( _x >= pos_x && _x <= pos_x + clips[0].w && _y >= pos_y && _y <= pos_y + clips[0].h )
    {
        use_index = BUTTON_HIGHLIGHTED;
        return true;
    }
    else
    {
        use_index = BUTTON_NORMAL;
        return false;
    }
}
void Button::show()
{
    apply_surface( pos_x, pos_y, sf_Button_Sprites, sf_Screen, &clips[ use_index ] );

    SDL_FreeSurface( sf_Buttontxt );
    sf_Buttontxt = TTF_RenderText_Solid( font_UI, text.c_str(), txtColor_white );

    shift_x = ( BTN_W - sf_Buttontxt->w ) / 2;
    shift_y = ( BTN_H - sf_Buttontxt->h ) / 2;

    apply_surface( pos_x + shift_x, pos_y + shift_y, sf_Buttontxt, sf_Screen );

}