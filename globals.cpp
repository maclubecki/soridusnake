#include "globals.h"

//RNG
int random_gen( int a, int b, int x = NULL )
{
    return ( a + ( rand() % ( b-a+1 ) ) );
}
//loading imgs
SDL_Surface* load_img( std::string filename )
{
    SDL_Surface* loaded = NULL;
    SDL_Surface* optimized = NULL;

    loaded = IMG_Load( filename.c_str() );

    if( loaded != NULL )
    {
        optimized = SDL_DisplayFormat( loaded );
        SDL_FreeSurface( loaded );
        SDL_SetColorKey( optimized, SDL_SRCCOLORKEY, SDL_MapRGB( optimized->format, 0, 0xFF, 0xFF ) );
    }

    return optimized;
}
//applying imgs
void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* dest , SDL_Rect* clip = NULL )
{
    SDL_Rect offset;

    offset.x = x;
    offset.y = y;

    SDL_BlitSurface( source, clip, dest, &offset);
}
//initializing all
bool init()
{
    //video
    if( SDL_Init( SDL_INIT_EVERYTHING ) == -1)
        return false;

    sf_Screen = SDL_SetVideoMode( WID, HIG, BPP, SDL_HWSURFACE );
    if( sf_Screen == NULL )
        return false;

    //fonts
    if( TTF_Init() == -1 )
    {
        return false;
    }

    //audio
    if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) == -1 )
    {
        return false;
    }

    SDL_WM_SetCaption( "SolidSnake", NULL );

    return true;
}
//mass load files
bool load_files()
{
    //loading images:
    //menu bg
    sf_Menubg = load_img( "./gfx/menu.png" );
    if( sf_Menubg == NULL )
        return false;
    //background
    sf_Background = load_img( "./gfx/bg.png" );
    if( sf_Background == NULL )
        return false;
    //button sprites
    sf_Button_Sprites = load_img( "./gfx/buttons.png");
    if( sf_Button_Sprites == NULL )
        return false;
    //tile sprite sheet
    sf_Tile_Sheet = load_img( "./gfx/tile_sheet.png");
    if( sf_Tile_Sheet == NULL )
        return false;
    //tile sprite sheet
    sf_Error = load_img( "./gfx/error.png");
    if( sf_Error == NULL )
        return false;

    //loading font
    font_UI = TTF_OpenFont( "visitor1.ttf", 18 );
    if( font_UI == NULL )
        return false;

    //death message
    std::stringstream t;
    t << "YOU ARE DEAD";
    sf_DeathText = TTF_RenderText_Solid( font_UI, t.str().c_str(), txtColor_white );

    /*
    //loading sound
    SFX_bump_pad = Mix_LoadWAV( "./sfx/pad.wav" );
    */

    return true;
}
//cleanup
void cleanup()
{
    //free surfaces
    SDL_FreeSurface( sf_Background );

    /*
    //close font and quit ttf
    TTF_CloseFont( font_score );
    TTF_Quit();

    //free all sound chunks and quit mixer
    Mix_FreeChunk( SFX_bump_pad );
    Mix_CloseAudio();
    */
    SDL_Quit();
}