#include "tile.h"

Tile::Tile()
{
    x = 0;
    y = 0;

    setType( TILE_EMPTY );

    clip.y = 0;
    clip.w = TILE_A;
    clip.h = TILE_A;
}
Tile::Tile( int _x, int _y, e_Tiletype _type )
{
    x = _x;
    y = _y;
    setType( _type );

    clip.y = 0;
    clip.w = TILE_A;
    clip.h = TILE_A;
}

int Tile::getX()
{
    return x;
}
int Tile::getY()
{
    return y;
}

e_Tiletype Tile::getType()
{
    return type;
}

void Tile::setX( int _x )
{
    x = _x;
}
void Tile::setY( int _y )
{
    y = _y;
}
void Tile::setType( e_Tiletype _newtype )
{
    type = _newtype;
    clip.x = type * TILE_A;
}
void Tile::show()
{
    apply_surface( x*TILE_A + STAGE_XSHIFT, y *TILE_A + STAGE_YSHIFT, sf_Tile_Sheet, sf_Screen, &clip );
}