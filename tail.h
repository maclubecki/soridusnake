#ifndef TAIL_H_INCLUDED
#define TAIL_H_INCLUDED
class Tail
{
    private:
        int id;
        Point position;

    public:

        Tail* next;
        Tail* prev;

        Tail( int _x, int _y );
        int getX();
        int getY();
        int getID();
        void setPosition( int _x, int _y );
        void setPrevious( Tail* _prev );
};

#endif // TAIL_H_INCLUDED
