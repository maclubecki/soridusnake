#include "timer.h"

Timer::Timer()
{
    start_time = 0;
    paused_time = 0;
    paused = false;
    running = false;
}
int Timer::getTicks()
{
    if( running )
    {
        if( paused )
            return pausedTicks;
        else
            return SDL_GetTicks() - startTicks;
    }

    return 0;
}

bool Timer::isStarted()
{
    return running;
}
bool Timer::isPaused()
{
    return paused;
}

void Timer::start()
{
    running = true;
    paused = false;
    startTime = SDL_GetTicks();
}

void Timer::stop()
{
    running = false;
    paused = false;
}

void Timer::pause()
{
    //If the timer is running and isn't already paused
    if( running && !paused )
    {
        paused = true;
        pausedTime = SDL_GetTicks() - startTime;
    }
}

void Timer::unpause()
{
    //If the timer is paused
    if( paused )
    {
        paused = false;
        //Reset the starting ticks
        startTime = SDL_GetTicks() - pausedTime;
        //Reset the paused ticks
        pausedTime = 0;
    }
}