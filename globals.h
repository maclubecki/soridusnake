#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include "SDL/SDL_mixer.h"
#include <string>
#include <cmath>
#include <vector>
#include <fstream>
#include <sstream>
#include "surfaces.h"

#ifndef GLOBALS_H_INCLUDED
#define GLOBALS_H_INCLUDED

//CONSTANTS
//screen measurements
const int WID = 600;
const int HIG = 650;
const int BPP =  32;
//button measures
const short BTN_W = 140;
const short BTN_H =  40;
//tile measures
const short TILE_A = 12;
//stage measures
const short STAGE_A = 35;
const short STAGE_XSHIFT = ( WID - ( TILE_A * STAGE_A ) ) / 2;
const short STAGE_YSHIFT = ( HIG - ( TILE_A * STAGE_A ) ) / 2;
//starting snake size
const short TAIL_SIZE = 5;
const short MSG_V = 300;

//STRUCTS
struct Point
{
    int x, y;
};

struct Food
{
    int x, y;
    bool eaten;
};

//ENUMS

//game states
enum e_Gamestate
{
    GAME_MENU,
    GAME_PLAYING,
    GAME_DEATH
};
//button states
enum e_Buttonstate
{
    BUTTON_NORMAL,
    BUTTON_HIGHLIGHTED
};
//tile types
enum e_Tiletype
{
    TILE_EMPTY,
    TILE_PLAYER,
    TILE_TAIL,
    TILE_PICKUP,
    TILE_BLOCK
};

//FUNCTIONS
//RNG
int random_gen( int a, int b, int x = NULL );
//loading imgs
SDL_Surface* load_img( std::string filename );
//applying imgs
void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* dest , SDL_Rect* clip = NULL );
//initializing all
bool init();
//mass load files
bool load_files();
//cleanup
void cleanup();
#endif // GLOBALS_H_INCLUDED
