#ifndef BUTTON_H_INCLUDED
#define BUTTON_H_INCLUDED

class Button
{
    private:

        int   pos_x,   pos_y;
        int shift_x, shift_y;
        int        use_index;
        bool    start_button;
        std::string     text;
        SDL_Rect    clips[2];//[1] is highlighted, [0] isnt

    public:

        Button( int _posX, int _posY, std::string _text );
        bool is_selected( int _x, int _y );
        void show();

};


#endif // BUTTON_H_INCLUDED
