#include "movable.h"

Movable::Movable()
{
    is_dead = false;
    score = 0;
    inputs = 0;

    tail_count = 1;
    tail_toadd = TAIL_SIZE;

    position.x = 1;
    position.y = 1;

    cur_direction.x = 1;
    cur_direction.y = 0;
}
int Movable::getX()
{
    return position.x;
}
int Movable::getY()
{
    return position.y;
}
int Movable::getXdir()
{
    return cur_direction.x;
}
int Movable::getYdir()
{
    return cur_direction.y;
}
int Movable::getTailCount()
{
    return tail_count;
}
int Movable::getToAdd()
{
    return tail_toadd;
}
bool Movable::getIsDead()
{
    return is_dead;
}
void Movable::setIsDead( bool _is_dead )
{
    is_dead = _is_dead;
}
void Movable::setToAdd( int x )
{
    tail_toadd = x;
}
void Movable::setTailCount( int x )
{
    tail_count = x;
}
void Movable::setDir( int _x, int _y )
{
    cur_direction.x = _x;
    cur_direction.y = _y;
}
void Movable::setPosition( int _x, int _y )
{
    position.x = _x;
    position.y = _y;
}
void Movable::reset()
{
    position.x = 1;
    position.y = 1;
    score = 0;
    setDir( 1, 0 );
    setTailCount( 1 );
    setToAdd( TAIL_SIZE );
    setIsDead( false );
}
void Movable::move( Tail* t, bool _add, bool collided, Tile* block, Food &grub )
{
        //the element passed to this function is the 'tip' of the tail
        //the tail segments move to their predecessor's position
        //the first element is different as it is determined wheter it moves or not, depending
        //on if it was just added

        //if dead, quit the function
        if( is_dead )
            return;

        //if it's not just a head
        if( t!= NULL )
        {
            //in case the snake has a tail length of 1
            if( t->prev != NULL )
            {
               if( !_add )
                {
                    t->setPosition( t->prev->getX(), t->prev->getY() );
                }
                t = t->prev;

                //then we have a loop that goes up the tail till it reaches the element that follows the head
                while( t->prev != NULL )
                {
                    t->setPosition( t->prev->getX(), t->prev->getY() );
                    t = t->prev;
                }
            }

            if( t->prev == NULL )
                t->setPosition( position.x, position.y );
        }

        //if the next tile getting occupied doesnt kill the player
        if( block->getType() == TILE_BLOCK || collided )
        {
            //kill
            setIsDead( true );
        }
        else
        {
            position.x += cur_direction.x;
            position.y += cur_direction.y;
            inputs = 0;

            if( position.x >= STAGE_A )
                position.x = 0;
            if( position.x < 0 )
                position.x = STAGE_A - 1;
            if( position.y >= STAGE_A )
                position.y = 0;
            if( position.y < 0 )
                position.y = STAGE_A - 1;

            //check if food has been eaten
            if( position.x == grub.x && position.y == grub.y )
            {
                tail_toadd++;
                grub.eaten = true;
                score++;
            }
        }
}
void Movable::handle_input( bool &quit )
{
    SDL_Event e;

    if( SDL_PollEvent( &e ) )
    {
        if( e.type == SDL_KEYDOWN && !is_dead && inputs == 0)
        {
            switch( e.key.keysym.sym )
            {
                case SDLK_LEFT:
                    if( cur_direction.x != 1 )
                        setDir(-1, 0);
                    break;

                case SDLK_RIGHT:
                    if( cur_direction.x != -1 )
                        setDir(1, 0);
                    break;

                case SDLK_UP:
                    if( cur_direction.y != 1 )
                        setDir(0, -1);
                    break;

                case SDLK_DOWN:
                    if( cur_direction.y != -1 )
                        setDir(0, 1);
                    break;
            }
            inputs++;
        }
        if( e.type == SDL_QUIT )
        {
            quit = true;
            return;
        }
    }
}