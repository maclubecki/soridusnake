#include "tail.h"

Tail::Tail( int _x, int _y )
{
    /*
    <- PREV     NEXT ->
    []ooooooooooooooooo
      ^first          ^last
    if prev == null - that means it's the first segment that follows the head
    if next == null - that means it's the ass
    */

    id = 0;

    position.x = _x;
    position.y = _y;

    prev = NULL;
    next = NULL;
}
int Tail::getX()
{
    return position.x;
}
int Tail::getY()
{
    return position.y;
}
int Tail::getID()
{
    return id;
}
void Tail::setPosition( int _x, int _y )
{
    position.x = _x;
    position.y = _y;
}
void Tail::setPrevious( Tail* _prev )
{
    prev = _prev;
    id = _prev->getID() + 1;
}